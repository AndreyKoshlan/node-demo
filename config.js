/* express configuration */
module.exports.connection_addr = '127.0.0.1'
module.exports.connection_port = 9000

/* database configuration */
module.exports.database_url = 'mongodb://127.0.0.1/storage'

/* validation */
module.exports.min_password_length = 6
module.exports.max_password_length = 64

/* read schema
 * list all public fields */
module.exports.read_schema = (user) => {
    return {
        'id': user['_id'],
        'email': user['email'],
        'name': user['name'],
        'surname': user['surname']
    }
}