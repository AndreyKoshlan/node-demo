const {body, validationResult} = require('express-validator')
const config = require('./config')
const database = require('./database.js')

Middleware = class {
    static sessionValidation() {
        return [
            body('sid', 'Invalid Session ID').custom(value => {
                return database.users.findBySessionId(value).then((user) => {
                    if (!user)
                        return Promise.reject()
                })
            })
        ]
    }

    static uniqueEmailValidation() {
        return [
            body('email','Email address already exists').custom(value => {
                return database.users.findByEmail(value).then((arr) => {
                    if (arr.length)
                        return Promise.reject()
                })
            })
        ]
    }

    static userEmailUpdateValidation() {
        return [
            body('email','Email address already exists').custom((value, {req}) => {
                return database.users.findBySessionId(req.body.sid).then((user) => {
                    if (user['email'] !== req.body.email) {
                        return database.users.findByEmail(value).then((arr) => {
                            if (arr.length)
                                return Promise.reject()
                        })
                    }
                })
            })
        ]
    }

    static userEmailValidation() {
        return [
            body('email', 'Invalid email address').isEmail()
        ]
    }

    static userPassValidation() {
        return [
            body('pass', 'Minimal password length is '+config.min_password_length)
                .isLength({min: config.min_password_length}),
            body('pass', 'Maximum password length is '+config.max_password_length)
                .isLength({max: config.max_password_length}),
        ]
    }

    static userNameValidation() {
        return [
            body('name').exists().matches('[a-zA-Z-]'),
            body('surname').exists().matches('[a-zA-Z-]')
        ]
    }

    static registrationUserValidation() {
        return [
            this.uniqueEmailValidation(), //field 'email'
            this.userEmailValidation(), //field 'email'
            this.userPassValidation(), //field 'pass'
            this.userNameValidation() //field 'name' & 'surname'
        ]
    }

    static updateValidation() {
        return [
            this.sessionValidation(), //field 'sid'
            this.resultValidation, //stop chain
            this.userEmailUpdateValidation(), //field 'email'
            this.userEmailValidation(), //field 'email'
            this.userPassValidation(), //field 'pass'
            this.userNameValidation() //field 'name' & 'surname'
        ]
    }

    static resultValidation(req, res, next) {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            res.status(400).json({
                errors: errors.mapped()
            });
            return next(400)
        }
        next()
    }

    static action(obj, func, errorStr, finalFunc) {
        return (req, res, next) => {
            func.bind(obj)(req.body).then((out) => {
                let string = finalFunc(out)
                if (string) {
                    res.send(string)
                    return next()
                } else {
                    res.status(400).send({ error: errorStr });
                    return next(400)
                }
            })
        }
    }
}

module.exports = Middleware