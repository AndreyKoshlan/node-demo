const mongoose = require('mongoose')
const pbkdf2 = require('pbkdf2')
const crypto = require('crypto');
const config = require('./config')

const UserSchema = new mongoose.Schema({
    email: {type: String, required: true, lowercase: true},
    pass: {type: String, required: true},
    name: {type: String, required: true},
    surname: {type: String, required: true}
});

const SessionSchema = new mongoose.Schema( {
    sid: {type: String, required: true},
    user: {type: String, required: true},
    createdAt: { type: Date, expires: '4w', default: Date.now }
})

UserModel = mongoose.model('Users', UserSchema);
SessionModel = mongoose.model('Sessions', SessionSchema)

Users = class {
    static _crypt_user_hash(pass) {
        //todo: salt from db?
        return pbkdf2.pbkdf2Sync(pass, '', 1000000, 64, 'sha512').toString('hex')
    }

    async _generate_session_key() {
        let sid = ''
        do {
            sid = crypto.randomBytes(96).toString('hex')
        } while(SessionModel.find({'sid': sid}).length)
        return sid
    }

    async findByEmail(email) {
        return UserModel.find({'email': email})
    }

    async findBySessionId(sid) {
        let session = await SessionModel.find({'sid': sid})
        let _id = session.length ? session[0]['user'] : undefined
        if (_id) {
            return UserModel.findById(_id)
        }
        return undefined
    }

    async auth(body) {
        let email = body['email']
        let hashPass = Users._crypt_user_hash(body['pass'])
        let user = await UserModel.find({'email': email, 'pass': hashPass})
        if (user.length) {
            let sid = await this._generate_session_key()
            let session = new SessionModel({'sid': sid, 'user': user[0]['_id']})
            await session.save()
            return sid
        }
        return ''
    }

    async update(body) {
        let sid = body['sid']
        let email = body['email']
        let hashPass = Users._crypt_user_hash(body['pass'])
        let name = body['name']
        let surname = body['surname']
        let user = await this.findBySessionId(body['sid'])
        let instance = new UserModel({_id: user._id, email: email, pass: hashPass, name: name, surname: surname})

        /* save to db */
        try {
            await user.updateOne(instance)
            await user.save()
            return true
        } catch {
            return false
        }
    }

    async register(body) {
        let email = body['email']
        let hashPass = Users._crypt_user_hash(body['pass'])
        let name = body['name']
        let surname = body['surname']
        let instance = new UserModel({email: email, pass: hashPass, name: name, surname: surname})

        /* save to db */
        try {
            await instance.save()
            return this.auth(body)
        } catch {
            return ''
        }
    }

    async delete(body) {
        let user = await this.findBySessionId(body['sid'])
        try {
            await SessionModel.deleteMany({'user': user._id})
            await user.deleteOne()
            return true
        } catch {
            return false
        }
    }

    async read(body) {
        let user = await UserModel.findById(body['uid']).then((user)=>{return user}).catch(()=>{return undefined})
        if (user) {
            return JSON.stringify(config.read_schema(user))
        }
        return undefined
    }

    async list(body) {
        let users = await UserModel.find({} )
        let from = Number(body['index'])
        let to = Number(body['index'])+Number(body['count'])-1
        if (from < 0) from = 0
        let list = []
        for (let i = from; i <= Math.min(users.length-1, to); i++) {
            list.push(JSON.stringify(config.read_schema(users[i])))
        }
        return list
    }
}

Database = class {
    constructor() {
        this.users = new Users()
        mongoose.connect(config.database_url, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true}).then(() => {
            this.connection = mongoose.connection
            this.connection.on('error', console.error.bind(console, 'MongoDB connection error:'))
        })
    }
}

module.exports = new Database()