const winston = require('winston')

function createLoggerRequestFormat() {
    return winston.format.combine(
        winston.format.timestamp(),
        winston.format.printf(info => {
            let {req, res, next} = info.message;
            let timestamp = info.timestamp
            let level = info.level
            let addr = req.socket.remoteAddress
            let port = req.socket.remotePort
            let method = req.method
            let url = req.url
            let body = req.body.length ? ` body: ${JSON.stringify(req.body)}` : ""
            let headers = ''
            Object.keys(req.headers).forEach(key => {headers += `${key}: ${req.headers[key]}\n`})
            next()
            return `[${timestamp}] ${level}: ${addr}:${port} - - "${method} ${url}${body}"\n${headers}`
        })
    )
}

function createLoggerRequest() {
    const msgLogger = winston.createLogger({
        level: 'info',
        transports: [new winston.transports.Console()],
        format: createLoggerRequestFormat()
    })
    return msgLogger
}

module.exports = {
    loggerRequest: createLoggerRequest(),
    //loggerError: createLoggerError()
}