const express = require('express')
const config = require('./config')
const routes = require('./routes')

const app = express()

Server = class {
    async start() {
        this.httpserver = app.listen(config.connection_port, config.connection_addr, () => {
            this._add_routes()
        })
    }

    _add_routes() {
        app.use(express.json())
        app.use(express.urlencoded({ extended: true }))

        routes.add_user_routes(app)
    }
}

module.exports = Server