const Server = require('./server')

srv = new Server()
srv.start().then(() => {
    const address = srv.httpserver.address().address
    const port = srv.httpserver.address().port
    console.log('Server started: %s:%d', address, port)
})