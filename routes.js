const middleware = require('./middleware')
const database = require('./database')

Routes = class {
    static add_user_routes(app) {
        /* register */
        app.post('/user/register/',
            middleware.registrationUserValidation(),
            middleware.resultValidation,
            middleware.action(database.users, database.users.register, 'Reg fail', (result) => {
                return result
            })
        )

        /* auth */
        app.post('/user/auth/',
            middleware.action(database.users, database.users.auth, 'Auth fail', (result) => {
                return result
            })
        )

        /* read */
        app.get('/user/read/',
            middleware.sessionValidation(),
            middleware.resultValidation,
            middleware.action(database.users, database.users.read, 'Read fail', (result) => {
                return result
            })
        )

        /* update */
        app.put('/user/update/',
            middleware.updateValidation(),
            middleware.resultValidation,
            middleware.action(database.users, database.users.update, 'Update fail', (result) => {
                if (result) return 'success'
            })
        )

        /* delete */
        app.delete('/user/delete/',
            middleware.sessionValidation(),
            middleware.resultValidation,
            middleware.action(database.users, database.users.delete, 'Delete fail', (result) => {
                if (result) return 'success'
            })
        )

        /* list */
        app.get('/user/list/',
            middleware.sessionValidation(),
            middleware.resultValidation,
            middleware.action(database.users, database.users.list, 'List fail', (result) => {
                return result
            })
        )
    }
}

module.exports = Routes